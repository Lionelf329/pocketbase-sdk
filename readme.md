# Pocketbase SDK

This project is meant to dramatically simplify the process of interacting with a backend
implemented using [PocketBase](https://github.com/pocketbase/pocketbase). It does this by
using KSP to generate all the glue needed to manage updates, syncing and offline access,
so all you need to do as a user is define your data models. I've been using it for my own
projects in the past that I can't share because they have private info in them, but I threw
together this demo to give an idea of how it works and the value it brings. Take a look in
`app/src/main/kotlin/dev/foxcroft/demo`, particularly in `MainActivity.kt` and `PbModels.kt`
to see sample usage, and feel free to try installing it to see how it works. I can't promise
that the demo will actually run perfectly because it was a bit of a last-minute thing and I
might have messed up a network security config thing somewhere, but it worked for me so
hopefully it'll work for you too.
