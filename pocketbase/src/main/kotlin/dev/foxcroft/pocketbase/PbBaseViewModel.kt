package dev.foxcroft.pocketbase

import android.content.Context
import android.net.ConnectivityManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.squareup.moshi.Moshi
import dev.foxcroft.pocketbase.auth.AuthRequester
import dev.foxcroft.pocketbase.auth.AuthStore
import dev.foxcroft.pocketbase.auth.AuthUseCase
import dev.foxcroft.pocketbase.db.DatabaseHelper
import dev.foxcroft.pocketbase.realtime.RtEvent
import dev.foxcroft.pocketbase.realtime.connectAndSubscribe
import dev.foxcroft.pocketbase.retrofit.PocketBase
import dev.foxcroft.pocketbase.sync.CollectionSyncHelper
import dev.foxcroft.pocketbase.sync.SyncHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import java.lang.Thread.sleep

private typealias VmBuilder<AuthRecord, ViewModel> = (
    connectivityManager: ConnectivityManager,
    db: DatabaseHelper,
    service: PocketBase,
    authUseCase: AuthUseCase<AuthRecord>,
) -> ViewModel

abstract class PbBaseViewModel<AuthRecord, PbRecord>(
    connectivityManager: ConnectivityManager,
    private val db: DatabaseHelper,
    private val service: PocketBase,
    private val authUseCase: AuthUseCase<AuthRecord>,
) : ViewModel() {

    companion object {
        inline fun <reified AuthRecord, Record, ViewModel> create(
            context: Context,
            config: AppConfig,
            moshi: Moshi,
            constructor: VmBuilder<AuthRecord, ViewModel>,
            authCollection: String,
            helpers: List<SyncHelper<out Record>>,
        ): ViewModel where ViewModel : PbBaseViewModel<AuthRecord, Record> {
            val connectivityManager =
                context.service<ConnectivityManager>(Context.CONNECTIVITY_SERVICE)

            val db = DatabaseHelper(
                context,
                config.databaseName,
                null,
                config.databaseVersion,
                helpers,
            )

            val service = PocketBase(moshi, config.baseUrl) { config.clientBuilder(context) }

            val authUseCase = AuthUseCase<AuthRecord>(
                AuthStore.create(context, moshi),
                AuthRequester.create(service, authCollection),
                config.clientId,
            )

            return constructor(connectivityManager, db, service, authUseCase)
        }
    }

    protected abstract val collectionSyncHelpers: List<CollectionSyncHelper<*>>
    val serviceReachable = networkState(connectivityManager, viewModelScope)
    val currentUser = authUseCase.currentUser
    val authInProgress = authUseCase.authInProgress

    init {
        onAppLaunch()
    }

    private fun onAppLaunch() {
        authUseCase.onCreate(viewModelScope)
        viewModelScope.launch(Dispatchers.IO) {
            while (true) {
                connectRealtime()
            }
        }
    }

    private suspend fun connectRealtime() {
        waitForAuth()
        try {
            coroutineScope {
                val collections = collectionSyncHelpers.map { it.collection }
                connectAndSubscribe(this, service.realtime, ::parseEvent, ::onEvent, collections)
                collectionSyncHelpers.forEach {
                    launch {
                        it.sync(::waitForAuth)
                    }
                }
            }
        } catch (_: InterruptedException) {
        } catch (_: IllegalStateException) {
            sleep(10_000)
        }
    }

    protected abstract fun parseEvent(event: String, payload: String): RtEvent<PbRecord>

    protected abstract fun onEvent(it: RtEvent<PbRecord>)

    private suspend fun waitForAuth() {
        currentUser
            .combine(serviceReachable) { user, reachable ->
                !user?.token.isNullOrEmpty() && reachable
            }.filter { it }
            .first()
    }

    fun login(context: Context) = viewModelScope.launch(Dispatchers.IO) {
        authUseCase.login(context)
    }

    override fun onCleared() {
        super.onCleared()
        db.close()
    }
}
