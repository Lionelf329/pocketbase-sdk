package dev.foxcroft.pocketbase.auth

import android.content.Context
import android.util.Log
import androidx.credentials.CredentialManager
import androidx.credentials.GetCredentialRequest
import com.google.android.libraries.identity.googleid.GetGoogleIdOption
import com.google.android.libraries.identity.googleid.GoogleIdTokenCredential
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import dev.foxcroft.pocketbase.retrofit.RecordsService.AuthRefreshResponse as RefreshResponse
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.time.Duration


class AuthUseCase<AuthRecord>(
    private val authStore: AuthStore<AuthRecord>,
    private val authRequester: AuthRequester<AuthRecord>,
    private val clientId: String,
) {
    companion object {
        private const val AUTH_TAG = "PB_AUTH"
        private val refreshThreshold = Duration.ofDays(13).toSeconds()
    }

    val currentUser = MutableStateFlow<RefreshResponse<AuthRecord>?>(null)
    val authInProgress = MutableStateFlow(false)

    fun onCreate(scope: CoroutineScope) {
        val initialAuth = authStore.getAuth()?.let(::setAuth)
        if (initialAuth != null && timeRemaining(initialAuth.token) < refreshThreshold) {
            scope.launch(Dispatchers.IO) {
                refreshAuth()
            }
        }
    }

    suspend fun login(context: Context) = withAuthInProgress {
        val jwt = getGoogleJwt(context)
        val authResponse = authRequester.authWithJwt(jwt)
        if (authResponse.isSuccessful) {
            val (token, record) = authResponse.body()!!
            setAuth(RefreshResponse(token, record))
        } else {
            Log.e(AUTH_TAG, "Auth request failed with code ${authResponse.code()}")
        }
    }

    private suspend fun refreshAuth() = withAuthInProgress {
        val authResponse = authRequester.authRefresh()

        if (authResponse.isSuccessful) {
            setAuth(authResponse.body()!!)
        } else {
            Log.e(AUTH_TAG, "Refresh request failed with code ${authResponse.code()}")
        }
    }

    private inline fun withAuthInProgress(block: () -> Unit) {
        if (authInProgress.value) return
        try {
            authInProgress.value = true
            block()
        } catch (e: Throwable) {
            Log.e(AUTH_TAG, e.stackTraceToString())
        } finally {
            authInProgress.value = false
        }
    }

    private fun setAuth(userData: RefreshResponse<AuthRecord>): RefreshResponse<AuthRecord> {
        val cleanUserData = when (timeRemaining(userData.token)) {
            0L -> userData.copy(token = "")
            else -> userData
        }
        authRequester.setToken(cleanUserData.token)
        currentUser.value = cleanUserData
        authStore.setAuth(cleanUserData)
        return cleanUserData
    }

    private suspend fun getGoogleJwt(context: Context): String {
        val googleIdOption = GetGoogleIdOption.Builder()
            .setFilterByAuthorizedAccounts(false)
            .setAutoSelectEnabled(true)
            .setServerClientId(clientId)
            .build()

        val credentialRequest = GetCredentialRequest.Builder()
            .addCredentialOption(googleIdOption)
            .build()

        val credentialData = CredentialManager
            .create(context)
            .getCredential(context, credentialRequest)
            .credential
            .data

        return GoogleIdTokenCredential.createFrom(credentialData).idToken
    }
}
