package dev.foxcroft.pocketbase.auth

import okhttp3.Interceptor


class AuthInterceptor : Interceptor {
    var credentials: String = ""

    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        val request = chain.request()
        return chain.proceed(
            if (credentials.isNotEmpty()) {
                request.newBuilder()
                    .header("Authorization", "Bearer $credentials")
                    .build()
            } else {
                request
            }
        )
    }
}
