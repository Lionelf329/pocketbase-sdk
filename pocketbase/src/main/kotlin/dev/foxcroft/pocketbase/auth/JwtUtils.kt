package dev.foxcroft.pocketbase.auth

import android.util.Base64
import dev.foxcroft.pocketbase.retrofit.RecordsService
import kotlin.math.max


suspend inline fun <reified T> RecordsService.authWithJwt(collection: String, token: String) =
    authWithOauth2<T>(
        collectionIdOrName = collection,
        provider = "google",
        code = token,
        redirectUrl = "Jwt",
    )

// Returns the number of seconds until the jwt expires, or 0 if anything goes wrong when parsing it
fun timeRemaining(jwt: String, now: Long = System.currentTimeMillis()): Long {
    val b64 = jwt.split(".").getOrNull(1) ?: return 0
    val exp = try {
        val str = Base64.decode(b64, 0).decodeToString()
        val match = Regex("\"exp\"\\s*:\\s*(\\d+)").find(str) ?: return 0
        val group = match.groups[1] ?: return 0
        group.value.toLong()
    } catch (_: Throwable) {
        return 0
    }
    return max(exp - now / 1000, 0)
}
