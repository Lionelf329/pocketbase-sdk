package dev.foxcroft.pocketbase.auth

import dev.foxcroft.pocketbase.retrofit.PocketBase
import dev.foxcroft.pocketbase.retrofit.RecordsService
import retrofit2.Response


interface AuthRequester<AuthRecord> {
    companion object {
        inline fun <reified AuthRecord> create(service: PocketBase, collection: String) =
            object : AuthRequester<AuthRecord> {
                override suspend fun authWithJwt(token: String) =
                    service.records.authWithJwt<AuthRecord>(collection, token)

                override suspend fun authRefresh() =
                    service.records.authRefresh<AuthRecord>(collection)

                override fun setToken(token: String) = service.setToken(token)
            }
    }

    suspend fun authWithJwt(token: String): Response<RecordsService.AuthWithOauth2Response<AuthRecord>>

    suspend fun authRefresh(): Response<RecordsService.AuthRefreshResponse<AuthRecord>>

    fun setToken(token: String)
}
