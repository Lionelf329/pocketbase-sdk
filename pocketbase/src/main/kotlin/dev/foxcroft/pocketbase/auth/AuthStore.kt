package dev.foxcroft.pocketbase.auth

import android.content.Context
import android.content.SharedPreferences
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapter
import dev.foxcroft.pocketbase.retrofit.RecordsService.AuthRefreshResponse


interface AuthStore<T> {
    companion object {
        const val FILE = "auth"
        const val USER = "user"

        inline fun <reified T> create(
            context: Context,
            moshi: Moshi,
            fileName: String = FILE,
        ) = object : AuthStore<T> {
            @OptIn(ExperimentalStdlibApi::class)
            private val adapter = moshi.adapter<AuthRefreshResponse<T>>()

            private val sharedPrefs: SharedPreferences =
                context.getSharedPreferences(fileName, Context.MODE_PRIVATE)

            override fun getAuth() = try {
                adapter.fromJson(sharedPrefs.getString(USER, "") ?: "")
            } catch (e: Throwable) {
                null
            }

            override fun setAuth(auth: AuthRefreshResponse<T>?) {
                with(sharedPrefs.edit()) {
                    when (val json = adapter.toJson(auth)) {
                        "null", null -> remove(USER)
                        else -> putString(USER, json)
                    }
                    apply()
                }
            }
        }
    }

    fun getAuth(): AuthRefreshResponse<T>?

    fun setAuth(auth: AuthRefreshResponse<T>?)
}
