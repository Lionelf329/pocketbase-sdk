package dev.foxcroft.pocketbase

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.launch

fun networkState(manager: ConnectivityManager, scope: CoroutineScope): StateFlow<Boolean> {
    val serviceReachable = MutableStateFlow(manager.isConnected())
    scope.launch(Dispatchers.Main) {
        watchNetwork(manager).collect {
            serviceReachable.value = it
        }
    }
    return serviceReachable
}

private fun watchNetwork(manager: ConnectivityManager): Flow<Boolean> = callbackFlow {
    val request = NetworkRequest.Builder()
        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        .build()

    val listener = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            trySend(true)
        }

        override fun onLost(network: Network) {
            trySend(false)
        }
    }
    manager.registerNetworkCallback(request, listener)
    awaitClose { manager.unregisterNetworkCallback(listener) }
}

private fun ConnectivityManager.isConnected() = activeNetwork
    ?.let(::getNetworkCapabilities)
    ?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
    ?: false

inline fun <reified T> Context.service(name: String) = getSystemService(name) as T
