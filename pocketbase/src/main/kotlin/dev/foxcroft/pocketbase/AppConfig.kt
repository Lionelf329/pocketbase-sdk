package dev.foxcroft.pocketbase

import android.content.Context
import okhttp3.OkHttpClient

data class AppConfig(
    val baseUrl: String,
    val clientId: String,
    val databaseName: String,
    val databaseVersion: Int,
    val clientBuilder: (Context) -> OkHttpClient.Builder = { OkHttpClient.Builder() },
)
