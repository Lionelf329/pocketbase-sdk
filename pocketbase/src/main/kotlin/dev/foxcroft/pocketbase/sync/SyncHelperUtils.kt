package dev.foxcroft.pocketbase.sync

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import dev.foxcroft.macros.record.BASE
import dev.foxcroft.macros.record.LOCAL
import dev.foxcroft.macros.record.REMOTE
import dev.foxcroft.macros.record.Record


fun <T> SQLiteDatabase.getItems(sql: String, getRecord: Cursor.(Int) -> T): List<T> {
    val items = mutableListOf<T>()
    val cursor = rawQuery(sql, arrayOf())
    while (cursor.moveToNext()) {
        items.add(cursor.getRecord(0))
    }
    cursor.close()
    return items
}

fun <T> SQLiteDatabase.getRecord(id: String, sql: String, getRecord: Cursor.(Int) -> T): Record<T> {
    var base: T? = null
    var remote: T? = null
    var local: T? = null
    val cursor = rawQuery(sql, arrayOf(id))
    while (cursor.moveToNext()) {
        val item = cursor.getRecord(1)
        when (cursor.getInt(0)) {
            BASE -> base = item
            REMOTE -> remote = item
            LOCAL -> local = item
            else -> throw IllegalStateException()
        }
    }
    cursor.close()
    return Record(base, remote, local)
}

fun <T> SQLiteDatabase.getRecords(sql: String, getRecord: Cursor.(Int) -> T): List<Record<T>> {
    var base: T? = null
    var remote: T? = null
    var local: T? = null
    var prevId: String? = null
    val cursor = rawQuery(sql, arrayOf())
    val ans = mutableListOf<Record<T>>()
    while (cursor.moveToNext()) {
        val id = cursor.getString(1)
        if (id != prevId) {
            if (prevId != null) {
                ans.add(Record(base, remote, local))
            }
            base = null
            remote = null
            local = null
            prevId = id
        }
        val item = cursor.getRecord(1)
        when (cursor.getInt(0)) {
            BASE -> base = item
            REMOTE -> remote = item
            LOCAL -> local = item
            else -> throw IllegalStateException()
        }
    }
    if (prevId != null) {
        ans.add(Record(base, remote, local))
    }
    cursor.close()
    return ans
}
