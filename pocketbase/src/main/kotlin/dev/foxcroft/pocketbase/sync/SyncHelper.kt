package dev.foxcroft.pocketbase.sync

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.core.database.sqlite.transaction
import dev.foxcroft.macros.record.STATE
import dev.foxcroft.macros.record.SyncInterface
import dev.foxcroft.pocketbase.db.setSyncState
import dev.foxcroft.pocketbase.retrofit.PocketBase
import okhttp3.MultipartBody
import java.time.ZonedDateTime


interface SyncHelper<T> :
    SyncInterface<SQLiteDatabase, ContentValues, PocketBase, MutableList<MultipartBody.Part>, T> {

    val createTableSql: String
    val dropTableSql get() = "DROP TABLE IF EXISTS $collection"
    val getRecordSql: String
    val getRecordsSql: String
    val getUpdatesSql: String
    val getConflictsSql: String

    fun readCursor(cursor: Cursor, i: Int = 0): T

    override fun insert(db: SQLiteDatabase, item: T, state: Int) {
        db.insert(collection, null, localPayload(item, state))
    }

    override fun update(db: SQLiteDatabase, values: ContentValues, id: String, state: Int?) {
        if (!values.isEmpty) {
            if (state == null) {
                db.update(collection, values, "id=?", arrayOf(id))
            } else {
                val params = arrayOf(id, state.toString())
                db.update(collection, values, "id=? AND $STATE=?", params)
            }
        }
    }

    override fun delete(db: SQLiteDatabase, id: String, states: List<Int>?) {
        if (states == null) {
            db.delete(collection, "id=?", arrayOf(id))
        } else {
            val condition = states.joinToString(" OR ") { "$STATE=$it" }
            db.delete(collection, "id=? AND ($condition)", arrayOf(id))
        }
    }

    override fun getRecord(db: SQLiteDatabase, id: String) =
        db.getRecord(id, getRecordSql, ::readCursor)

    override fun getItems(db: SQLiteDatabase) = db.getItems(getRecordsSql, ::readCursor)

    override fun getUpdates(db: SQLiteDatabase) = db.getRecords(getUpdatesSql, ::readCursor)

    override fun getConflicts(db: SQLiteDatabase) = db.getRecords(getConflictsSql, ::readCursor)

    override fun <T> transaction(db: SQLiteDatabase, block: SQLiteDatabase.() -> T) =
        db.transaction(body = block)

    override fun setSyncState(db: SQLiteDatabase, state: Pair<ZonedDateTime, String>) {
        db.setSyncState(collection, state)
    }
}
