package dev.foxcroft.pocketbase.sync

import android.util.Log
import dev.foxcroft.macros.record.Record
import dev.foxcroft.pocketbase.db.DatabaseHelper
import dev.foxcroft.pocketbase.db.getSyncState
import dev.foxcroft.pocketbase.retrofit.PocketBase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import java.net.ConnectException
import java.net.SocketTimeoutException

private const val SYNC_TAG = "PB_SYNC"

class CollectionSyncHelper<T>(
    private val helper: SyncHelper<T>,
    private val db: DatabaseHelper,
    private val service: PocketBase,
) {
    val collection = helper.collection
    val objects = MutableStateFlow(helper.getItems(db.readableDatabase).associateBy(helper::id))
    private val lastWrite = MutableStateFlow(System.currentTimeMillis())

    suspend fun sync(waitForAuth: suspend () -> Unit) {
        val collection = helper.collection
        Log.d(SYNC_TAG, "Pulling $collection")
        val db = db.writableDatabase
        helper.pull(db, service, db.getSyncState()[helper.collection]) { records ->
            records.forEach { (id, record) -> notifyWrite(id, record) }
        }
        Log.d(SYNC_TAG, "Finished pulling $collection")
        var lastWriteCompleted = 0L
        while (true) {
            val nextWrite = lastWrite
                .filter { it > lastWriteCompleted }
                .first()
            waitForAuth()
            try {
                Log.d(SYNC_TAG, "Pushing $collection")
                helper.push(db, service)
                Log.d(SYNC_TAG, "Finished pushing $collection")
                lastWriteCompleted = nextWrite
            } catch (_: ConnectException) {
                Log.e(SYNC_TAG, "Pushing $collection failed because of connection error")
            } catch (_: SocketTimeoutException) {
                Log.e(SYNC_TAG, "Pushing $collection failed because of a connection timeout")
            } catch (e: Throwable) {
                Log.e(SYNC_TAG, e.stackTraceToString())
                throw IllegalStateException()
            }
        }
    }

    fun putRemote(incoming: T) {
        val id = helper.id(incoming)
        val recordAfter = helper.putRemote(db.writableDatabase, incoming)
        notifyWrite(id, recordAfter)
    }

    fun putLocal(prev: T?, record: T) {
        val recordAfter = helper.putLocal(db.writableDatabase, prev, record)
        notifyWrite(helper.id(record), recordAfter)
    }

    private fun notifyWrite(id: String, record: Record<T>) {
        if (record.local != null && record.base == null) {
            lastWrite.value = System.currentTimeMillis()
        }
        val item = record.local ?: record.remote
        if (objects.value[id] != item) {
            val prevState = objects.value.toMutableMap()
            when (item) {
                null -> prevState.remove(id)
                else -> prevState[id] = item
            }
            objects.value = prevState
        }
    }
}
