package dev.foxcroft.pocketbase.retrofit

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapter
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


@OptIn(ExperimentalStdlibApi::class)
inline fun <reified T> Response<ResponseBody>.convert(moshi: Moshi): Response<T> = map {
    moshi.adapter<T>().fromJson(it.string())!!
}

fun <From, To> Response<From>.map(f: (From) -> To): Response<To> = if (isSuccessful) {
    Response.success(f(body()!!), raw())
} else {
    Response.error(errorBody()!!, raw())
}

inline fun <reified T> makeService(moshi: Moshi, client: OkHttpClient, baseUrl: String): T =
    Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .client(client)
        .build()
        .create(T::class.java)
