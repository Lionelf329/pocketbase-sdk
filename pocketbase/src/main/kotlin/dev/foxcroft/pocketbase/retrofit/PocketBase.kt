package dev.foxcroft.pocketbase.retrofit

import com.squareup.moshi.Moshi
import dev.foxcroft.pocketbase.auth.AuthInterceptor
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Streaming
import java.util.concurrent.TimeUnit.MINUTES


class RecordsService(val moshi: Moshi, client: OkHttpClient, baseUrl: String) {
    val service: Service = makeService(moshi, client, baseUrl)

    interface Service {
        @GET("/api/collections/{collectionIdOrName}/records")
        suspend fun listRecords(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Query("page") page: Number? = null,
            @Query("perPage") perPage: Number? = null,
            @Query("sort") sort: String? = null,
            @Query("filter") filter: String? = null,
            @Query("expand") expand: String? = null,
            @Query("fields") fields: String? = null,
            @Query("skipTotal") skipTotal: Boolean? = null,
        ): Response<ResponseBody> // Response<ListRecordsResponse<T>>

        @GET("/api/collections/{collectionIdOrName}/records/{recordId}")
        suspend fun viewRecord(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Path("recordId") recordId: String,
            @Query("expand") expand: String? = null,
            @Query("fields") fields: String? = null,
        ): Response<ResponseBody> // Response<T>

        @Multipart
        @POST("/api/collections/{collectionIdOrName}/records")
        suspend fun createRecord(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Query("expand") expand: String? = null,
            @Query("fields") fields: String? = null,
            @Part body: List<MultipartBody.Part>,
        ): Response<ResponseBody> // Response<T>

        @Multipart
        @PATCH("/api/collections/{collectionIdOrName}/records/{recordId}")
        suspend fun updateRecord(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Path("recordId") recordId: String,
            @Query("expand") expand: String? = null,
            @Query("fields") fields: String? = null,
            @Part body: List<MultipartBody.Part>,
        ): Response<ResponseBody> // Response<T>

        @DELETE("/api/collections/{collectionIdOrName}/records/{recordId}")
        suspend fun deleteRecord(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Path("recordId") recordId: String,
        ): Response<Unit>

        @GET("/api/collections/{collectionIdOrName}/auth-methods")
        suspend fun listAuthMethods(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Query("fields") fields: String? = null,
        ): Response<ListAuthMethodsResponse>

        @POST("/api/collections/{collectionIdOrName}/auth-with-password")
        suspend fun authWithPassword(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Query("expand") expand: String? = null,
            @Query("fields") fields: String? = null,
            @Body body: AuthWithPasswordBody,
        ): Response<ResponseBody> // Response<AuthWithPasswordResponse<T>>

        data class AuthWithPasswordBody(
            val identity: String?,
            val password: String?,
        )

        @POST("/api/collections/{collectionIdOrName}/auth-with-oauth2")
        suspend fun authWithOauth2(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Query("expand") expand: String? = null,
            @Query("fields") fields: String? = null,
            @Body body: AuthWithOauth2Body,
        ): Response<ResponseBody> // Response<AuthWithOauth2Response<T>>

        data class AuthWithOauth2Body(
            val provider: String?,
            val code: String?,
            val codeVerifier: String?,
            val redirectUrl: String?,
            val createData: Any?,
        )

        @POST("/api/collections/{collectionIdOrName}/auth-refresh")
        suspend fun authRefresh(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Query("expand") expand: String? = null,
            @Query("fields") fields: String? = null,
        ): Response<ResponseBody> // Response<AuthRefreshResponse<T>>

        @POST("/api/collections/{collectionIdOrName}/request-verification")
        suspend fun requestVerification(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Body body: RequestVerificationBody,
        ): Response<Unit>

        data class RequestVerificationBody(
            val email: String?,
        )

        @POST("/api/collections/{collectionIdOrName}/confirm-verification")
        suspend fun confirmVerification(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Body body: ConfirmVerificationBody,
        ): Response<Unit>

        data class ConfirmVerificationBody(
            val token: String?,
        )

        @POST("/api/collections/{collectionIdOrName}/request-password-reset")
        suspend fun requestPasswordReset(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Body body: RequestPasswordResetBody,
        ): Response<Unit>

        data class RequestPasswordResetBody(
            val email: String?,
        )

        @POST("/api/collections/{collectionIdOrName}/confirm-password-reset")
        suspend fun confirmPasswordReset(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Body body: ConfirmPasswordResetBody,
        ): Response<Unit>

        data class ConfirmPasswordResetBody(
            val token: String?,
            val password: String?,
            val passwordConfirm: String?,
        )

        @POST("/api/collections/{collectionIdOrName}/request-email-change")
        suspend fun requestEmailChange(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Body body: RequestEmailChangeBody,
        ): Response<Unit>

        data class RequestEmailChangeBody(
            val newEmail: String?,
        )

        @POST("/api/collections/{collectionIdOrName}/confirm-email-change")
        suspend fun confirmEmailChange(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Body body: ConfirmEmailChangeBody,
        ): Response<Unit>

        data class ConfirmEmailChangeBody(
            val token: String?,
            val password: String?,
        )

        @GET("/api/collections/{collectionIdOrName}/records/{id}/external-auths")
        suspend fun listLinkedExternalAuthProviders(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Path("id") id: String,
            @Query("fields") fields: String? = null,
        ): Response<List<LinkedExternalAuthProvider>>

        @DELETE("/api/collections/{collectionIdOrName}/records/{id}/external-auths/{provider}")
        suspend fun unlinkExternalAuthProvider(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Path("id") id: String,
            @Path("provider") provider: String,
        ): Response<Unit>
    }

    suspend inline fun <reified T> listRecords(
        collectionIdOrName: String,
        page: Number? = null,
        perPage: Number? = null,
        sort: String? = null,
        filter: String? = null,
        expand: String? = null,
        fields: String? = null,
        skipTotal: Boolean? = null,
    ): Response<ListRecordsResponse<T>> = service.listRecords(
        collectionIdOrName,
        page,
        perPage,
        sort,
        filter,
        expand,
        fields,
        skipTotal,
    ).convert(moshi)

    data class ListRecordsResponse<T>(
        val page: Int,
        val perPage: Int,
        val totalItems: Int,
        val totalPages: Int,
        val items: List<T>,
    )

    suspend inline fun <reified T> viewRecord(
        collectionIdOrName: String,
        recordId: String,
        expand: String? = null,
        fields: String? = null,
    ): Response<T> = service.viewRecord(
        collectionIdOrName,
        recordId,
        expand,
        fields,
    ).convert(moshi)

    suspend inline fun <reified T> createRecord(
        collectionIdOrName: String,
        expand: String? = null,
        fields: String? = null,
        body: List<MultipartBody.Part>,
    ): Response<T> = service.createRecord(
        collectionIdOrName,
        expand,
        fields,
        body,
    ).convert(moshi)

    suspend inline fun <reified T> updateRecord(
        collectionIdOrName: String,
        recordId: String,
        expand: String? = null,
        fields: String? = null,
        body: List<MultipartBody.Part>,
    ): Response<T> = service.updateRecord(
        collectionIdOrName,
        recordId,
        expand,
        fields,
        body,
    ).convert(moshi)

    suspend fun deleteRecord(
        collectionIdOrName: String,
        recordId: String,
    ): Response<Unit> = service.deleteRecord(
        collectionIdOrName,
        recordId,
    )

    suspend fun listAuthMethods(
        collectionIdOrName: String,
        fields: String? = null,
    ): Response<ListAuthMethodsResponse> = service.listAuthMethods(
        collectionIdOrName,
        fields,
    )

    data class ListAuthMethodsResponse(
        val usernamePassword: Boolean,
        val emailPassword: Boolean,
        val authProviders: List<AuthProvider>,
    )

    data class AuthProvider(
        val name: String,
        val state: String,
        val codeVerifier: String,
        val codeChallenge: String,
        val codeChallengeMethod: String,
        val authUrl: String,
    )

    suspend inline fun <reified T> authWithPassword(
        collectionIdOrName: String,
        expand: String? = null,
        fields: String? = null,
        identity: String? = null,
        password: String? = null,
    ): Response<AuthWithPasswordResponse<T>> = service.authWithPassword(
        collectionIdOrName,
        expand,
        fields,
        Service.AuthWithPasswordBody(identity, password),
    ).convert(moshi)

    data class AuthWithPasswordResponse<T>(
        val token: String,
        val record: T,
    )

    suspend inline fun <reified T> authWithOauth2(
        collectionIdOrName: String,
        expand: String? = null,
        fields: String? = null,
        provider: String? = null,
        code: String? = null,
        codeVerifier: String? = null,
        redirectUrl: String? = null,
        createData: Any? = null,
    ): Response<AuthWithOauth2Response<T>> = service.authWithOauth2(
        collectionIdOrName,
        expand,
        fields,
        Service.AuthWithOauth2Body(provider, code, codeVerifier, redirectUrl, createData),
    ).convert(moshi)

    data class AuthWithOauth2Response<T>(
        val token: String,
        val record: T,
        val meta: Meta,
    )

    data class Meta(
        val id: String,
        val name: String,
        val username: String,
        val email: String,
        val isNew: Boolean,
        val avatarUrl: String,
        val rawUser: Any?,
        val accessToken: String,
        val refreshToken: String,
        val expiry: String,
    )

    suspend inline fun <reified T> authRefresh(
        collectionIdOrName: String,
        expand: String? = null,
        fields: String? = null,
    ): Response<AuthRefreshResponse<T>> = service.authRefresh(
        collectionIdOrName,
        expand,
        fields,
    ).convert(moshi)

    data class AuthRefreshResponse<T>(
        val token: String,
        val record: T,
    )

    suspend fun requestVerification(
        collectionIdOrName: String,
        email: String? = null,
    ): Response<Unit> = service.requestVerification(
        collectionIdOrName,
        Service.RequestVerificationBody(email),
    )

    suspend fun confirmVerification(
        collectionIdOrName: String,
        token: String? = null,
    ): Response<Unit> = service.confirmVerification(
        collectionIdOrName,
        Service.ConfirmVerificationBody(token),
    )

    suspend fun requestPasswordReset(
        collectionIdOrName: String,
        email: String? = null,
    ): Response<Unit> = service.requestPasswordReset(
        collectionIdOrName,
        Service.RequestPasswordResetBody(email),
    )

    suspend fun confirmPasswordReset(
        collectionIdOrName: String,
        token: String? = null,
        password: String? = null,
        passwordConfirm: String? = null,
    ): Response<Unit> = service.confirmPasswordReset(
        collectionIdOrName,
        Service.ConfirmPasswordResetBody(token, password, passwordConfirm),
    )

    suspend fun requestEmailChange(
        collectionIdOrName: String,
        newEmail: String? = null,
    ): Response<Unit> = service.requestEmailChange(
        collectionIdOrName,
        Service.RequestEmailChangeBody(newEmail),
    )

    suspend fun confirmEmailChange(
        collectionIdOrName: String,
        token: String? = null,
        password: String? = null,
    ): Response<Unit> = service.confirmEmailChange(
        collectionIdOrName,
        Service.ConfirmEmailChangeBody(token, password),
    )

    suspend fun listLinkedExternalAuthProviders(
        collectionIdOrName: String,
        id: String,
        fields: String? = null,
    ): Response<List<LinkedExternalAuthProvider>> = service.listLinkedExternalAuthProviders(
        collectionIdOrName,
        id,
        fields,
    )

    data class LinkedExternalAuthProvider(
        val id: String,
        val created: String,
        val updated: String,
        val recordId: String,
        val collectionId: String,
        val provider: String,
        val providerId: String,
    )

    suspend fun unlinkExternalAuthProvider(
        collectionIdOrName: String,
        id: String,
        provider: String,
    ): Response<Unit> = service.unlinkExternalAuthProvider(
        collectionIdOrName,
        id,
        provider,
    )
}

class RealtimeService(
    val moshi: Moshi,
    client: OkHttpClient,
    streamingClient: OkHttpClient,
    baseUrl: String,
) {
    private val service: Service = makeService(moshi, client, baseUrl)
    private val streamingService: StreamingService = makeService(moshi, streamingClient, baseUrl)

    interface Service {
        @POST("/api/realtime")
        suspend fun setSubscriptions(
            @Body body: SetSubscriptionsBody,
        ): Response<Unit>

        data class SetSubscriptionsBody(
            val clientId: String?,
            val subscriptions: List<String>?,
        )
    }

    interface StreamingService {
        @GET("/api/realtime")
        @Streaming
        fun connect(): Call<ResponseBody>
    }

    fun connect(): Call<ResponseBody> = streamingService.connect()

    suspend fun setSubscriptions(
        clientId: String? = null,
        subscriptions: List<String>? = null,
    ): Response<Unit> = service.setSubscriptions(
        Service.SetSubscriptionsBody(clientId, subscriptions),
    )
}

class FilesService(val moshi: Moshi, client: OkHttpClient, baseUrl: String) {
    private val service: Service = makeService(moshi, client, baseUrl)

    interface Service {
        @GET("/api/files/{collectionIdOrName}/{recordId}/{filename}")
        suspend fun downloadFile(
            @Path("collectionIdOrName") collectionIdOrName: String,
            @Path("recordId") recordId: String,
            @Path("filename") filename: String,
            @Query("thumb") thumb: String? = null,
            @Query("token") token: String? = null,
            @Query("download") download: Boolean? = null,
        ): Response<ResponseBody>

        @POST("/api/files/token")
        suspend fun generateProtectedFileToken(): Response<GenerateProtectedFileTokenResponse>
    }

    suspend fun downloadFile(
        collectionIdOrName: String,
        recordId: String,
        filename: String,
        thumb: String? = null,
        token: String? = null,
        download: Boolean? = null,
    ): Response<ResponseBody> = service.downloadFile(
        collectionIdOrName,
        recordId,
        filename,
        thumb,
        token,
        download,
    )

    suspend fun generateProtectedFileToken(): Response<String> =
        service.generateProtectedFileToken().map { it.token }

    data class GenerateProtectedFileTokenResponse(
        val token: String,
    )
}

class HealthService(val moshi: Moshi, client: OkHttpClient, baseUrl: String) {
    private val service: Service = makeService(moshi, client, baseUrl)

    interface Service {
        @GET("/api/health")
        suspend fun healthCheck(
            @Query("fields") fields: String? = null,
        ): Response<HealthCheckResponse>
    }

    suspend fun healthCheck(
        fields: String? = null,
    ): Response<HealthCheckResponse> = service.healthCheck(
        fields,
    )

    data class HealthCheckResponse(
        val code: Int,
        val message: String,
        val data: Data,
    )

    data class Data(
        val canBackup: Boolean,
    )
}

class PocketBase(
    moshi: Moshi,
    baseUrl: String,
    clientBuilder: () -> OkHttpClient.Builder = OkHttpClient::Builder
) {
    private val interceptor = AuthInterceptor()
    private val client = clientBuilder().addInterceptor(interceptor).build()
    private val streamingClient = clientBuilder().readTimeout(5, MINUTES).build()

    fun setToken(token: String) {
        interceptor.credentials = token;
    }

    val records = RecordsService(moshi, client, baseUrl)
    val realtime = RealtimeService(moshi, client, streamingClient, baseUrl)
    val files = FilesService(moshi, client, baseUrl)
    val health = HealthService(moshi, client, baseUrl)
}
