package dev.foxcroft.pocketbase.realtime

import android.util.Log
import com.squareup.moshi.Json
import dev.foxcroft.pocketbase.retrofit.RealtimeService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import kotlin.coroutines.resume
import kotlin.math.min


const val REALTIME_TAG = "PB_REALTIME"

private const val ID_PREFIX = "id:"
private const val EVENT_PREFIX = "event:"
private const val DATA_PREFIX = "data:"
private const val CONNECT_EVENT = "PB_CONNECT"

sealed class Event<out T>
private data class Connect(val id: String) : Event<Nothing>()
data class RtEvent<out T>(val record: T, val action: EventAction) : Event<T>()

enum class EventAction {
    @Json(name = "create")
    Create,

    @Json(name = "delete")
    Delete,

    @Json(name = "update")
    Update,
}

suspend fun <T> connectAndSubscribe(
    scope: CoroutineScope,
    realtimeService: RealtimeService,
    parseEvent: (event: String, payload: String) -> RtEvent<T>,
    onEvent: (RtEvent<T>) -> Unit,
    collections: List<String>,
) {
    Log.d(REALTIME_TAG, "Connecting to SSE")
    val id = sseConnection(scope, realtimeService, parseEvent, onEvent)
    Log.d(REALTIME_TAG, "Subscribing to collections")
    subscribe(realtimeService, id, collections)
}

private fun <T> getFlow(
    service: RealtimeService,
    parseEvent: (String, String) -> RtEvent<T>,
) = flow {
    coroutineScope {
        val response = service.connect().execute()
        if (response.isSuccessful) {
            response.body()!!.byteStream().bufferedReader().use { input ->
                var id: String? = null
                var event: String? = null
                while (isActive) {
                    val line = input.readLine() ?: continue
                    if (line.isNotEmpty()) {
                        if (line.startsWith(ID_PREFIX)) {
                            id = line.substring(ID_PREFIX.length)
                        } else if (line.startsWith(EVENT_PREFIX)) {
                            event = line.substring(EVENT_PREFIX.length)
                        } else if (line.startsWith(DATA_PREFIX)) {
                            val data = line.substring(DATA_PREFIX.length)
                            if (event == CONNECT_EVENT) {
                                emit(Connect(id!!))
                            } else {
                                emit(parseEvent(event!!, data))
                            }
                            id = null
                            event = null
                        } else {
                            throw Error(line)
                        }
                    }
                }
            }
        } else {
            throw HttpException(response)
        }
    }
}.flowOn(Dispatchers.IO)

private suspend fun <T> sseConnection(
    scope: CoroutineScope,
    realtimeService: RealtimeService,
    parseEvent: (event: String, payload: String) -> RtEvent<T>,
    onEvent: (RtEvent<T>) -> Unit,
) = suspendCancellableCoroutine { c ->
    scope.launch {
        try {
            getFlow(realtimeService, parseEvent).collect { event ->
                when (event) {
                    is Connect -> c.resume(event.id)

                    is RtEvent -> withContext(Dispatchers.Main) {
                        onEvent(event)
                    }
                }
            }
        } catch (_: ConnectException) {
            Log.w(REALTIME_TAG, "Unable to establish realtime connection")
            throw IllegalStateException()
        } catch (_: SocketTimeoutException) {
            Log.w(REALTIME_TAG, "SSE connection timed out")
            throw InterruptedException()
        } catch (e: Throwable) {
            Log.e(REALTIME_TAG, e.stackTraceToString())
            throw IllegalStateException()
        }
    }
}

private suspend fun subscribe(
    realtimeService: RealtimeService,
    id: String,
    collections: List<String>,
) {
    for (duration in backoffSequence()) {
        try {
            val response = realtimeService.setSubscriptions(id, collections)
            if (response.isSuccessful) {
                return
            }
            val code = response.code()
            Log.w(REALTIME_TAG, "Failed to set subscriptions: $code")
            if (listOf(401, 403, 404).contains(code)) {
                throw IllegalStateException()
            }
        } catch (_: ConnectException) {
            Log.w(REALTIME_TAG, "Unable to connect while setting subscriptions")
        } catch (_: SocketTimeoutException) {
            Log.w(REALTIME_TAG, "Setting subscriptions timed out")
        } catch (e: Throwable) {
            Log.e(REALTIME_TAG, e.stackTraceToString())
            throw IllegalStateException()
        }
        Thread.sleep(duration)
    }
}

fun backoffSequence() = generateSequence(500L) { min(it * 2L, 8_000L) }
