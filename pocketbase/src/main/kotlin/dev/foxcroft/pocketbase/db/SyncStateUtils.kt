package dev.foxcroft.pocketbase.db

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import java.time.ZonedDateTime
import dev.foxcroft.pocketbase.ZonedDateTimeAdapter

const val CREATE_SYNC =
    "CREATE TABLE _sync(name TEXT NOT NULL PRIMARY KEY, dt TEXT NOT NULL, id TEXT NOT NULL)"
const val DROP_SYNC = "DROP TABLE IF EXISTS _sync"
private const val QUERY_SYNC = "SELECT name, dt, id FROM _sync"

fun SQLiteDatabase.getSyncState(): Map<String, Pair<ZonedDateTime, String>> = buildMap {
    val adapter = ZonedDateTimeAdapter()
    val cursor = rawQuery(QUERY_SYNC, arrayOf())
    while (cursor.moveToNext()) {
        val dt = adapter.fromJson(cursor.getString(1))!!
        val id = cursor.getString(2)
        put(cursor.getString(0), Pair(dt, id))
    }
    cursor.close()
}

fun SQLiteDatabase.setSyncState(name: String, value: Pair<ZonedDateTime, String>) {
    val adapter = ZonedDateTimeAdapter()
    val values = ContentValues().apply {
        put("dt", adapter.toJson(value.first))
        put("id", value.second)
    }
    update("_sync", values, "name=?", arrayOf(name))
}
