package dev.foxcroft.pocketbase.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import dev.foxcroft.pocketbase.sync.SyncHelper

class DatabaseHelper(
    context: Context,
    name: String,
    factory: SQLiteDatabase.CursorFactory?,
    version: Int,
    private val helpers: List<SyncHelper<*>>
) : SQLiteOpenHelper(context, name, factory, version) {

    override fun onCreate(db: SQLiteDatabase) {
        helpers.forEach {
            db.execSQL(it.createTableSql)
        }
        db.execSQL(CREATE_SYNC)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        helpers.forEach {
            db.execSQL(it.dropTableSql)
        }
        db.execSQL(DROP_SYNC)
        onCreate(db)
    }
}
