package dev.foxcroft.pocketbase

import dev.foxcroft.pocketbase.retrofit.PocketBase
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.security.SecureRandom
import java.time.ZonedDateTime


fun MutableList<MultipartBody.Part>.addParam(name: String, value: String) = apply {
    val requestBody = RequestBody.create(MediaType.get("text/plain"), value)
    add(MultipartBody.Part.createFormData(name, null, requestBody))
}

suspend inline fun <reified T> PocketBase.getUpdates(
    collection: String,
    after: Pair<ZonedDateTime, String>
): List<T>? {
    val (dt, id) = after
    val updated = ZonedDateTimeAdapter().toJson(dt)
    return records.listRecords<T>(
        collectionIdOrName = collection,
        sort = "updated,id",
        filter = "updated > \"$updated\" || updated = \"$updated\" && id > \"$id\"",
        perPage = 100,
        skipTotal = true
    ).body()?.items
}

fun generateId() = buildString {
    val random = SecureRandom()
    repeat(15) {
        append(random.nextInt(36).toString(36))
    }
}
