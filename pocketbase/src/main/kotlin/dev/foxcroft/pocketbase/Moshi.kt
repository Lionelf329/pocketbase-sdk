package dev.foxcroft.pocketbase

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.ToJson
import java.time.LocalDate
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


private val dtFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSX")

class ZonedDateTimeAdapter {
    @FromJson
    fun fromJson(string: String): ZonedDateTime? = try {
        ZonedDateTime.parse(string, dtFormatter)
    } catch (_: Throwable) {
        null
    }

    @ToJson
    fun toJson(value: ZonedDateTime?): String? = value?.format(dtFormatter)
}

class LocalDateAdapter {
    @FromJson
    fun fromJson(string: String): LocalDate? = try {
        LocalDate.parse(string, DateTimeFormatter.ISO_DATE)
    } catch (_: Throwable) {
        null
    }

    @ToJson
    fun toJson(value: LocalDate?): String? = value?.format(DateTimeFormatter.ISO_DATE)
}

class ObjectAdapter<T>(private val value: T) : JsonAdapter<T>() {
    override fun toJson(writer: JsonWriter, value: T?) {
        writer.beginObject()
        writer.endObject()
    }

    override fun fromJson(reader: JsonReader): T? {
        reader.skipValue()
        return value
    }
}
