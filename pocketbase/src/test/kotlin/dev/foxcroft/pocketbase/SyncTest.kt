package dev.foxcroft.pocketbase

import android.database.sqlite.SQLiteDatabase
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dev.foxcroft.macros.PocketbaseMoshi
import dev.foxcroft.macros.generate.PocketbaseRecord
import dev.foxcroft.macros.record.Record
import dev.foxcroft.pocketbase.sync.SyncHelper
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.equals.shouldBeEqual
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.time.ZonedDateTime

val dtAdapter = ZonedDateTimeAdapter()
val defaultDate = dtAdapter.fromJson("2024-01-01 00:00:00.000Z")!!

@PocketbaseMoshi
val moshi: Moshi = Moshi.Builder().addLast(KotlinJsonAdapterFactory()).add(dtAdapter).build()

@PocketbaseRecord("test")
data class TestRecord(
    val id: String = "id",
    val created: ZonedDateTime = defaultDate,
    val updated: ZonedDateTime = defaultDate,
    val deleted: Boolean = false,
    val string1: String,
    val string2: String,
)

@RunWith(RobolectricTestRunner::class)
class SyncTest {
    @Test
    fun test() = wrapTest(TestRecordHelper) { db ->
        db.execSQL(createTableSql)
        val aa = TestRecord(string1 = "A", string2 = "A")
        val ab = TestRecord(string1 = "A", string2 = "B")
        val ba = TestRecord(string1 = "B", string2 = "A")
        val bb = TestRecord(string1 = "B", string2 = "B")
        val cb = TestRecord(string1 = "C", string2 = "B")

        // Create a record
        putLocal(db, null, aa).shouldBeEqual(Record(base = null, remote = null, local = aa))

        // Sync it to the backend
        putRemote(db, aa).shouldBeEqual(Record(base = null, remote = aa, local = null))

        // Attempt to create it again, should throw
        shouldThrow<IllegalStateException> {
            putLocal(db, null, ba)
        }
        getRecord(db, aa.id).shouldBeEqual(Record(base = null, remote = aa, local = null))

        // Update one of its fields locally
        putLocal(db, aa, ba).shouldBeEqual(Record(base = null, remote = aa, local = ba))

        // Update the other field remotely
        putRemote(db, ab).shouldBeEqual(Record(base = null, remote = ab, local = bb))

        // Pull the value again, which should be a no-op
        putRemote(db, ab).shouldBeEqual(Record(base = null, remote = ab, local = bb))

        // Update the first field again locally, which was never synced after the first edit
        putLocal(db, bb, cb).shouldBeEqual(Record(base = null, remote = ab, local = cb))

        // Update the first field to a different value remotely, which causes a conflict
        putRemote(db, bb).shouldBeEqual(Record(base = ab, remote = bb, local = cb))

        // Resolve the conflict by accepting the remote version
        putLocal(db, ab, bb).shouldBeEqual(Record(base = null, remote = bb, local = null))
    }
}

private fun<T> wrapTest(helper: SyncHelper<T>, test: SyncHelper<T>.(SQLiteDatabase) -> Unit) {
    SQLiteDatabase.openDatabase(":memory:", null, 0).use { db ->
        helper.test(db)
    }
}
