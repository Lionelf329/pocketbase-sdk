from bs4 import BeautifulSoup
from json import loads
from re import fullmatch, sub
from requests import session
from typing import NamedTuple, Optional


class Param(NamedTuple):
    prefix: str
    name: str
    data_type: str
    default: Optional[str] = None

    def __str__(self):
        param = f'{self.prefix} {self.name}: {self.data_type}'.strip()
        return f'{param}' if self.default is None else f'{param} = {self.default}'


class DataClass(NamedTuple):
    name: str
    generic: bool
    params: list[Param]

    def __str__(self):
        generic = '<T>' if self.generic else ''
        return f'data class {self.name}{generic}({param_str(self.params)})'


class Endpoint(NamedTuple):
    multipart: bool
    method: str
    path: str
    streaming: bool
    generic: bool
    name: str
    params: list[Param]
    return_type: str
    body_class: Optional[DataClass]
    return_classes: list[DataClass]

    def __str__(self):
        modifiers = '@Multipart\n' if self.multipart else ''
        modifiers += f'@{self.method}("{self.path}")\n'
        params = list(self.params)
        if self.multipart:
            params.append(Param('@Part', 'body', 'List<MultipartBody.Part>'))
        elif self.body_class is not None:
            params.append(Param('@Body', 'body', self.body_class.name))
        params = param_str(params)
        if self.streaming:
            modifiers += f'@Streaming\n'
        else:
            modifiers += f'suspend '
        function = modifiers + f'fun {self.name}({params}): {self.return_type}'
        if self.body_class is not None:
            function += f'\n\n{self.body_class}'
        return function

    def str(self):
        params = list(self.params)
        call_params = [p.name for p in self.params]
        if self.multipart:
            params.append(Param('@Part', 'body', 'List<MultipartBody.Part>'))
            call_params.append('body')
        elif self.body_class is not None:
            params += [p._replace(prefix='', default='null') for p in self.body_class.params]
            call_params.append(f'Service.{self.body_class.name}({", ".join(p.name for p in self.body_class.params)})')
        params_str = param_str([p._replace(prefix="") for p in params])
        modifiers = '' if self.streaming else 'suspend '
        call_params_str = param_str(call_params)
        return_type = self.return_type.split('//')[-1].strip()
        function_core = f'{self.name}({params_str}): {return_type}'
        service = 'streamingService' if self.streaming else 'service'
        service_call = f'{service}.{self.name}({call_params_str})'
        if self.generic:
            function = modifiers + f'inline fun <reified T> {function_core} = {service_call}.convert(moshi)'
        elif self.return_classes and len(self.return_classes[0].params) == 1:
            return_class = self.return_classes[0]
            return_param = return_class.params[0]
            function_core = function_core.replace(return_class.name, return_param.data_type)
            function = modifiers + f'fun {function_core} =\n    {service_call}.map {{ it.{return_param.name} }}'
        else:
            function = modifiers + f'fun {function_core} = {service_call}'
        return function + ''.join(f'\n\n{c}' for c in self.return_classes)


class Service(NamedTuple):
    name: str
    endpoints: list[Endpoint]

    def add_route(self, a):
        if 'pb.admins' in a.text:
            return
        is_streaming = 'Establishes a new SSE connection' in a.text
        method, path = parse_path(a.find('div', class_='api-route'))
        route_name = transform_name(a.contents[0].text)
        params, body, multipart = self.get_params(a, uppercase(route_name) + 'Body')
        return_type, types, generic = self.get_types(a, route_name, is_streaming)
        endpoint = Endpoint(
            multipart,
            method,
            path,
            is_streaming,
            generic,
            route_name,
            params,
            return_type,
            body,
            types,
        )
        self.endpoints.append(endpoint)

    @staticmethod
    def get_params(a, body_name: str):
        params = []
        body = None
        multipart = False
        for table, title in zip(a.find_all('table'), a.find_all('div', class_='section-title')):
            if title.text == 'Path parameters':
                for row in table.find('tbody').find_all('tr'):
                    name, data_type, _ = row.find_all('td')
                    params.append(Param(path_annotation(name.text), name.text, data_type.text))
            elif title.text == 'Query parameters':
                for row in table.find('tbody').find_all('tr'):
                    name, data_type, _ = row.find_all('td')
                    params.append(Param(query_annotation(name.text), name.text, nullable(data_type.text), 'null'))
            elif title.text == 'Body Parameters':
                body_params = []
                for row in table.find('tbody').find_all('tr'):
                    cells = row.find_all('td')
                    if len(cells) == 3:
                        name_td, data_type, _ = cells
                        name = name_td.contents[0].contents[-1].text
                        body_params.append(Param('val', name, transform_type(data_type.text)))
                    else:
                        multipart = True
                body_params = list(dict.fromkeys(body_params))
                if not multipart:
                    body = DataClass(body_name, False, body_params)
        return params, body, multipart

    def get_types(self, a, route_name, is_streaming):
        if is_streaming:
            return 'Call<ResponseBody>', [], False

        if a.find('button', class_='tab-item', string=lambda text: text is not None and '204' in text):
            return 'Response<Unit>', [], False

        tabs = a.find_all('div', class_='tabs-content')[-1].contents
        json = tabs[0].text.strip().replace('{...}', '\"Any\"')
        if json == '[file resource]':
            return 'Response<ResponseBody>', [], False
        json = loads(json)

        if route_name.startswith('list') and isinstance(json, list):
            response_name = route_name[4:]
        else:
            response_name = uppercase(route_name) + 'Response'

        types = []
        type_name, generic = make_type(json, response_name, types)
        types.reverse()
        type_name = f'Response<ResponseBody> // Response<{type_name}>' if generic else f'Response<{type_name}>'
        return type_name, types, generic

    def __str__(self):
        private = 'private ' if not any(endpoint.generic for endpoint in self.endpoints) else ''
        service_def = f'    {private}val service: Service = makeService(moshi, client, baseUrl)\n'
        interface = (
                f'interface Service {{\n' +
                '\n\n'.join(indent(str(endpoint)) for endpoint in self.endpoints if not endpoint.streaming) +
                '\n}'
        )
        params = 'val moshi: Moshi, client: OkHttpClient, baseUrl: String'
        if self.has_streaming():
            service_def += f'    private val streamingService: StreamingService = makeService(moshi, streamingClient, baseUrl)\n'
            interface += (
                    f'\n\ninterface StreamingService {{\n' +
                    '\n\n'.join(indent(str(endpoint)) for endpoint in self.endpoints if endpoint.streaming) +
                    '\n}'
            )
            params = param_str([
                'val moshi: Moshi', 'client: OkHttpClient', 'streamingClient: OkHttpClient', 'baseUrl: String'
            ])
        return f'class {self.name}({params}) {{\n{service_def}\n' \
               f'{indent(interface)}\n\n' \
            + '\n\n'.join(indent(endpoint.str()) for endpoint in self.endpoints) \
            + '\n}'

    def __bool__(self):
        return bool(self.endpoints)

    def has_streaming(self):
        return any(endpoint.streaming for endpoint in self.endpoints)


class KotlinFile(NamedTuple):
    header: str
    services: list[Service]

    def add_pages(self, pages):
        for page in pages:
            self.add_page(page)

    def add_page(self, page):
        service_name = get_service_name(page)
        service = Service(f'{service_name}Service', [])
        for r in page.find_all('div', class_='accordion'):
            service.add_route(r)
        if service:
            self.services.append(service)

    def __str__(self):
        ans = self.header + '\n'
        for i in self.services:
            ans += '\n\n'
            ans += str(i)
        ans += '\n\nclass PocketBase(moshi: Moshi, baseUrl: String) {\n' \
               '    private val interceptor = AuthInterceptor()\n' \
               '    private val client = OkHttpClient.Builder().addInterceptor(interceptor).build()\n' \
               '    private val streamingClient = OkHttpClient.Builder().readTimeout(5, MINUTES).build()\n\n' \
               '    fun setToken(token: String) {\n' \
               '        interceptor.credentials = token;\n' \
               '    }\n\n'
        for i in self.services:
            name = i.name[0].lower() + i.name[1:-7]
            if i.has_streaming():
                ans += f'    val {name} = {i.name}(moshi, client, streamingClient, baseUrl)\n'
            else:
                ans += f'    val {name} = {i.name}(moshi, client, baseUrl)\n'
        return ans + '}\n'


def get_pages():
    s = session()
    root = soup(s.get('https://pocketbase.io/docs/api-records/'))
    links = [f'https://pocketbase.io{a.get("href")}' for a in root.find_all('a', class_='sub-list-item')]
    return [soup(s.get(link)) for link in links]


def get_service_name(page) -> str:
    names = [
        fullmatch(r'Web APIs reference - API (.+)', e.text)
        for e in page.find_all('div', class_='breadcrumb-item')
    ]
    return [n.group(1) for n in names if n is not None][0]


def soup(response):
    return BeautifulSoup(response.text, features='html.parser')


def indent(s: str, indent_level=1):
    padding = '    ' * indent_level
    return '\n'.join(padding + line if line else '' for line in s.split('\n'))


def transform_name(name: str):
    words = sub(r'\s*/\s*', '/', name).strip().split(' ')
    words = [w.split('/')[0].lower() for w in words]
    return ''.join(w if i == 0 else w[0].upper() + w[1:] for i, w in enumerate(words))


def uppercase(name: str):
    return name[0].upper() + name[1:]


def make_type(json, name: str, accumulator: list[DataClass]):
    if name == 'Record' or isinstance(json, dict) and 'title' in json:
        return 'T', True
    if json == 'Any':
        return 'Any?', False
    elif isinstance(json, str):
        return 'String', False
    elif isinstance(json, bool):
        return 'Boolean', False
    elif isinstance(json, int):
        return 'Int', False
    elif isinstance(json, list):
        if name.endswith('s'):
            name = name[:-1]
        item, generic = make_type(json[0], name, accumulator)
        return f'List<{item}>', generic
    elif isinstance(json, dict):
        fields = []
        generic = False
        for k, v in reversed(json.items()):
            field = ''
            type_name = uppercase(k)
            type_name, child_is_generic = make_type(v, type_name, accumulator)
            field += f'\n    val {k}: {type_name},'
            generic = generic or child_is_generic
            fields.append(Param('val', k, type_name))
        accumulator.append(DataClass(name, generic, list(reversed(fields))))
        return name + ('<T>' if generic else ''), generic
    else:
        print(json)
        exit(1)


def parse_path(route):
    return (
        route.contents[0].text,
        ''.join(f'{{{p.text}}}' if p.name == 'code' else p.text for p in route.contents[2].contents).strip(),
    )


def param_str(params: list):
    return indent("".join(f'\n{p},' for p in params)) + ('\n' if params else '')


def path_annotation(name: str):
    return f'@Path("{name}")'


def query_annotation(name: str):
    return f'@Query("{name}")'


def nullable(data_type: str):
    return f'{data_type}?'


def transform_type(type_name: str):
    type_map = {'null|String': 'String?', 'Object': 'Any', 'Array': 'List<Any>'}
    return type_map.get(type_name, type_name).replace('Array', 'List') + '?'


def write_file(path: str, contents: any):
    with open(path, 'w') as f:
        f.write(str(contents))


header = '''
package dev.foxcroft.pocketbase.retrofit

import com.squareup.moshi.Moshi
import dev.foxcroft.pocketbase.auth.AuthInterceptor
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Streaming
import java.util.concurrent.TimeUnit.MINUTES
'''.strip()


def main():
    kotlin_file = KotlinFile(header, [])
    kotlin_file.add_pages(get_pages())
    write_file('src/main/kotlin/dev/foxcroft/pocketbase/retrofit/PocketBase.kt', kotlin_file)


if __name__ == '__main__':
    main()
