package dev.foxcroft.demo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

class DiffCallback<ITEM : Any>(
    private val compareItems: (oldItem: ITEM, newItem: ITEM) -> Boolean,
    private val compareContents: (oldItem: ITEM, newItem: ITEM) -> Boolean
) : DiffUtil.ItemCallback<ITEM>() {
    override fun areItemsTheSame(oldItem: ITEM, newItem: ITEM): Boolean =
        compareItems(oldItem, newItem)

    override fun areContentsTheSame(oldItem: ITEM, newItem: ITEM): Boolean =
        compareContents(oldItem, newItem)
}

class SingleViewTypeListAdapter<ITEM : Any, VIEW> private constructor(
    list: MutableList<ITEM>,
    private val inflate: (LayoutInflater, ViewGroup, Boolean) -> VIEW,
    private val getRoot: (VIEW) -> View,
    private val bindRow: VIEW.(item: ITEM, position: Int) -> Unit,
    compareItems: (old: ITEM, new: ITEM) -> Boolean,
    compareContents: (old: ITEM, new: ITEM) -> Boolean,
    private val onClick: ((item: ITEM, position: Int) -> Unit)? = null
) : ListAdapter<ITEM, SingleViewTypeListAdapter<ITEM, VIEW>.ViewHolder>(
    DiffCallback(compareItems, compareContents)
) {
    init {
        submitList(list)
    }

    companion object {
        fun <ITEM : Any, VIEW : ViewBinding> withBinding(
            list: MutableList<ITEM>,
            inflate: (LayoutInflater, ViewGroup, Boolean) -> VIEW,
            bindRow: VIEW.(item: ITEM, position: Int) -> Unit,
            compareItems: (old: ITEM, new: ITEM) -> Boolean = { a, b -> a == b },
            compareContents: (old: ITEM, new: ITEM) -> Boolean = { a, b -> a == b },
            onClick: ((item: ITEM, position: Int) -> Unit)? = null
        ) = SingleViewTypeListAdapter(
            list = list,
            inflate = inflate,
            getRoot = { it.root },
            bindRow = bindRow,
            compareItems = compareItems,
            compareContents = compareContents,
            onClick = onClick
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    inner class ViewHolder(private val binding: VIEW) : RecyclerView.ViewHolder(getRoot(binding)) {
        fun bind(item: ITEM, position: Int) {
            binding.bindRow(item, position)
            onClick?.let { onClick ->
                itemView.setOnClickListener {
                    onClick(item, position)
                }
            }
        }
    }
}
