package dev.foxcroft.demo

import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import dev.foxcroft.demo.databinding.ActivityMainBinding
import dev.foxcroft.demo.databinding.ItemBinding
import kotlinx.coroutines.launch

class MainActivity : FragmentActivity() {
    private var _binding: ActivityMainBinding? = null
    private val binding: ActivityMainBinding get() = _binding!!

    private val viewModel by viewModelBuilder {
        PbViewModel.create(this, DEFAULT_CONFIG)
    }

    private val adapter by lazy {
        SingleViewTypeListAdapter.withBinding(
            list = mutableListOf<Item>(),
            inflate = ItemBinding::inflate,
            bindRow = { item, _ ->
                name.text = item.name
                count.text = item.count.toString()
            },
            compareItems = { a, b -> a.id == b.id },
            compareContents = { a, b -> a.name == b.name && a.count == b.count },
        ) { item, _ ->
            // This is all you need to do to update an item in the database
            viewModel.putLocal(item, item.copy(count = item.count + 1))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.recyclerView.adapter = adapter
        binding.login.setOnClickListener {
            // This is all you need to do to login with google
            viewModel.login(this)
        }
        binding.add.setOnClickListener {
            // This is all you need to do to create an item in the database
            viewModel.putLocal(
                null,
                Item(
                    name = "New item ${System.currentTimeMillis()}",
                    count = 0,
                    user = viewModel.currentUser.value!!.record.id,
                )
            )
        }
        lifecycleScope.launch {
            repeatOnLifecycle(androidx.lifecycle.Lifecycle.State.STARTED) {
                // This is all you need to do to observe changes in the database
                viewModel.items.collect {
                    adapter.submitList(it.values.toList())
                }
            }
        }
        lifecycleScope.launch {
            repeatOnLifecycle(androidx.lifecycle.Lifecycle.State.STARTED) {
                // This is all you need to do to observe changes to authentication status
                viewModel.currentUser.collect {
                    binding.login.isVisible = it == null
                    binding.add.isVisible = it != null
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}

inline fun <reified VM : ViewModel> FragmentActivity.viewModelBuilder(
    crossinline builder: () -> VM,
) = viewModels<VM> {
    object : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>) = builder() as T
    }
}
