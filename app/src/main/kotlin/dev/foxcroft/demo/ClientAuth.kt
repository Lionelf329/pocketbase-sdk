package dev.foxcroft.demo

import android.content.Context
import androidx.annotation.RawRes
import okhttp3.OkHttpClient
import java.security.KeyStore
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager


fun OkHttpClient.Builder.clientAuth(context: Context, @RawRes cert: Int): OkHttpClient.Builder {
    val keyStore = KeyStore.getInstance("PKCS12").apply {
        load(context.resources.openRawResource(cert), charArrayOf())
    }

    val keyManagers = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm()).apply {
        init(keyStore, charArrayOf())
    }.keyManagers

    val trustManagers = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm()).apply {
        init(null as KeyStore?)
    }.trustManagers

    val socketFactory = SSLContext.getInstance("TLS").apply {
        init(keyManagers, trustManagers, null)
    }.socketFactory

    val x509TrustManager = trustManagers[0] as X509TrustManager

    return sslSocketFactory(socketFactory, x509TrustManager)
}
