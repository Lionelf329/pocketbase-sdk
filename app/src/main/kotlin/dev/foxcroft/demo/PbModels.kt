package dev.foxcroft.demo

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dev.foxcroft.macros.PocketbaseMoshi
import dev.foxcroft.macros.generate.PocketbaseRecord
import dev.foxcroft.pocketbase.AppConfig
import dev.foxcroft.pocketbase.LocalDateAdapter
import dev.foxcroft.pocketbase.ZonedDateTimeAdapter
import dev.foxcroft.pocketbase.generateId
import okhttp3.OkHttpClient
import java.time.ZonedDateTime

val DEFAULT_CONFIG = AppConfig(
    baseUrl = "https://lionel.foxcroft.dev:8090",
    clientId = "489655361047-naglckj2lf47ovij92h9sr7uhuq9ijce.apps.googleusercontent.com",
    databaseName = "app.db",
    databaseVersion = 1,
) { OkHttpClient.Builder().clientAuth(it, R.raw.client_certificate) }

@PocketbaseMoshi
val moshi: Moshi = Moshi
    .Builder()
    .addLast(KotlinJsonAdapterFactory())
    .add(ZonedDateTimeAdapter())
    .add(LocalDateAdapter())
    .build()

sealed class PbRecord

@PocketbaseRecord("users", isAuth = true)
data class User(
    val id: String = generateId(),
    val created: ZonedDateTime = ZonedDateTime.now(),
    val updated: ZonedDateTime = ZonedDateTime.now(),
    val username: String,
    val email: String,
    val emailVisibility: Boolean,
    val verified: Boolean,
    val name: String,
    val avatar: String,
) : PbRecord()

@PocketbaseRecord("items")
data class Item(
    val id: String = generateId(),
    val created: ZonedDateTime = ZonedDateTime.now(),
    val updated: ZonedDateTime = ZonedDateTime.now(),
    val name: String,
    val count: Int,
    val user: String,
) : PbRecord()
