package dev.foxcroft.macros

import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.google.devtools.ksp.symbol.KSType
import dev.foxcroft.macros.generate.lowercase
import dev.foxcroft.macros.record.getTypeName

data class Serde(
    val name: String,
    val sqliteType: String,
    val serializeLocal: String,
    val serializeRemote: String,
    val deserialize: String,
)

fun serde(
    className: String,
    properties: List<KSPropertyDeclaration>,
): Pair<List<Serde>, List<KSType>> {
    val adapters = mutableListOf<KSType>()
    val serde = properties.map { property ->
        val propertyName = property.simpleName.asString()
        val notNull = !property.type.resolve().isMarkedNullable
        var sqliteType = ""
        var serializeLocal = ""
        var serializeRemote = ""
        var deserialize = ""

        fun nativeType(name: String, parse: String, sr: String) {
            if (notNull) {
                sqliteType = "$name NOT NULL"
                deserialize = "cursor.$parse"
            } else {
                sqliteType = name
                deserialize = "if (cursor.isNull(%d)) null else cursor.$parse"
            }
            serializeLocal = propertyName
            serializeRemote = sr
        }

        when (property.getTypeName()) {
            "kotlin.Boolean" -> nativeType("INTEGER", "getInt(%d) == 1", "record.$propertyName.toString()")
            "kotlin.Byte" -> nativeType("INTEGER", "getShort(%d).toByte()", "record.$propertyName.toString()")
            "kotlin.Short" -> nativeType("INTEGER", "getShort(%d)", "record.$propertyName.toString()")
            "kotlin.Int" -> nativeType("INTEGER", "getInt(%d)", "record.$propertyName.toString()")
            "kotlin.Long" -> nativeType("INTEGER", "getLong(%d)", "record.$propertyName.toString()")
            "kotlin.Float" -> nativeType("REAL", "getFloat(%d)", "record.$propertyName.toString()")
            "kotlin.Double" -> nativeType("REAL", "getDouble(%d)", "record.$propertyName.toString()")
            "kotlin.String" -> nativeType("TEXT", "getString(%d)", "record.$propertyName")
            "kotlin.ByteArray" -> nativeType("BLOB", "getBlob(%d)", TODO())
            else -> {
                val type = property.type.resolve()
                val adapter = when (val i = adapters.indexOf(type)) {
                    -1 -> {
                        adapters.add(type)
                        adapters.size - 1
                    }

                    else -> i
                }.let { adapterIndex ->
                    adapterName(className, adapterIndex)
                }
                sqliteType = "TEXT NOT NULL"
                serializeLocal = "$adapter.toJson($propertyName) ?: \"null\""
                serializeRemote = "$adapter.toJson(record.$propertyName) ?: \"null\""
                deserialize = "$adapter.fromJson(cursor.getString(%d))".let {
                    if (notNull) "$it!!" else it
                }
            }
        }
        Serde(
            name = propertyName,
            sqliteType = sqliteType,
            serializeLocal = "values.put(\"$propertyName\", $serializeLocal)",
            serializeRemote = serializeRemote,
            deserialize = "$propertyName = $deserialize"
        )
    }
    return Pair(serde, adapters)
}

fun adapterName(className: String, adapterIndex: Int) = "${className.lowercase}Adapter$adapterIndex"
