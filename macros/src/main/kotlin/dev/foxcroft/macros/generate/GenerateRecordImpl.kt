package dev.foxcroft.macros.generate

import com.google.devtools.ksp.getDeclaredProperties
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSType
import com.squareup.kotlinpoet.BOOLEAN
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.INT
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.STRING
import com.squareup.kotlinpoet.ksp.toClassName
import com.squareup.kotlinpoet.ksp.toTypeName
import dev.foxcroft.macros.Serde
import dev.foxcroft.macros.record.BASE
import dev.foxcroft.macros.record.LOCAL
import dev.foxcroft.macros.record.STATE
import dev.foxcroft.macros.serde


@Target(AnnotationTarget.CLASS)
annotation class PocketbaseRecord(val collection: String, val isAuth: Boolean = false)

data class RecordDef(
    val classDef: KSClassDeclaration,
    private val annotation: PocketbaseRecord,
    val serde: List<Serde>,
    val adapters: List<KSType>,
) {
    companion object {
        fun create(classDef: KSClassDeclaration): RecordDef {
            val annotation = classDef.getAnnotation<PocketbaseRecord>()
            val (serde, adapters) = serde(
                classDef.simpleName.asString(),
                classDef.getDeclaredProperties().toList(),
            )
            return RecordDef(classDef, annotation, serde, adapters)
        }
    }

    val collection = annotation.collection
    val isAuth = annotation.isAuth
    val quotedTableName = "\"$collection\""
    val packageName = classDef.qualifiedName?.getQualifier().orEmpty()
    val className = classDef.simpleName.asString()
    val classType = classDef.toClassName()
    val helper = "${className}Helper"
}

fun generateRecordImpl(r: RecordDef, moshi: ClassName) = fileSpec(r.packageName, r.className) {
    addOptInAnnotation()
    addImport(moshi.packageName, moshi.simpleName)
    addImport("com.squareup.moshi", "adapter")
    addImport("dev.foxcroft.macros.record", "EPOCH")
    addImport("dev.foxcroft.pocketbase", "addParam")
    addImport("dev.foxcroft.pocketbase", "getUpdates")

    r.adapters.forEachIndexed { i, t ->
        addLazyAdapter(
            moshi.simpleName,
            r.className,
            i,
            t.toTypeName(),
            t.toString()
        )
    }

    addObject(r.helper) {
        addSuperinterface(Types.SyncHelper(r.classType))

        addProperty("collection", STRING, KModifier.OVERRIDE) {
            initializer("\"${r.collection}\"")
        }

        addStringProperty("createTableSql") {
            append("CREATE TABLE ${r.quotedTableName} (")
            append("\n  \"$STATE\" INTEGER NOT NULL,")
            r.serde.forEach {
                append("\n  \"${it.name}\" ${it.sqliteType},")
            }
            append("\n  PRIMARY KEY(id, $STATE)")
            append("\n)")
        }

        addStringProperty("getRecordSql") {
            append("SELECT")
            append("\n  \"$STATE\"")
            r.serde.forEach {
                append(",\n  \"${it.name}\"")
            }
            append("\nFROM")
            append("\n  ${r.quotedTableName}")
            append("\nWHERE")
            append("\n  id = ?")
        }

        addStringProperty("getRecordsSql") {
            append("SELECT")
            r.serde.forEachIndexed { i, it ->
                if (i != 0) {
                    append(",")
                }
                append("\n  t.\"${it.name}\"")
            }
            append("\nFROM")
            append("\n  ${r.quotedTableName} t,")
            append("\n  (SELECT id, max($STATE) as $STATE FROM ${r.quotedTableName} GROUP BY id) i")
            append("\nWHERE")
            append("\n  t.id = i.id AND t.$STATE = i.$STATE")
        }

        addStringProperty("getUpdatesSql") {
            append("SELECT")
            append("\n  \"$STATE\"")
            r.serde.forEach {
                append(",\n  t.\"${it.name}\"")
            }
            append("\nFROM")
            append("\n  ${r.quotedTableName} t,")
            append("\n  (SELECT id FROM ${r.quotedTableName} GROUP BY id HAVING SUM($STATE = $LOCAL) > 0 AND SUM($STATE = $BASE) = 0) i")
            append("\nWHERE")
            append("\n  t.id = i.id")
            append("\nORDER BY")
            append("\n  t.id")
        }

        addStringProperty("getConflictsSql") {
            append("SELECT")
            append("\n  \"$STATE\"")
            r.serde.forEach {
                append(",\n  t.\"${it.name}\"")
            }
            append("\nFROM")
            append("\n  ${r.quotedTableName} t,")
            append("\n  (SELECT id FROM ${r.quotedTableName} WHERE $STATE = $BASE) i")
            append("\nWHERE")
            append("\n  t.id = i.id")
            append("\nORDER BY")
            append("\n  t.id")
        }

        addFunction("readCursor") {
            addModifiers(KModifier.OVERRIDE)
            addParameter("cursor", Types.Cursor)
            addParameter("i", INT)
            returns(r.classType)
            addCode(buildString {
                append("return ${r.className}(\n")
                r.serde.forEachIndexed { i, it ->
                    val deserialize = it.deserialize.replace("%d", if (i > 0) "i + $i" else "i")
                    append("  $deserialize,\n")
                }
                append(")")
            })
        }

        addFunction("id") {
            addModifiers(KModifier.OVERRIDE)
            addParameter("item", r.classType)
            returns(STRING)
            addCode("return item.id")
        }

        addFunction("updated") {
            addModifiers(KModifier.OVERRIDE)
            addParameter("item", r.classType)
            returns(Types.ZonedDateTime)
            addCode("return item.updated")
        }

        addFunction("conflictBase") {
            addModifiers(KModifier.OVERRIDE)
            addParameter("item", r.classType)
            returns(r.classType)
            addCode("return item.copy(created=EPOCH)")
        }

        addFunction("areEqual") {
            addModifiers(KModifier.OVERRIDE)
            addParameter("self", r.classType)
            addParameter("other", r.classType)
            returns(BOOLEAN)
            addCode("return (" + r.serde
                .filter { !listOf("id", "created", "updated").contains(it.name) }
                .joinToString(" &&") { "\n  self.${it.name} == other.${it.name}" } + "\n)"
            )
        }

        addFunction("hasConflict") {
            addModifiers(KModifier.OVERRIDE)
            addParameter("x", r.classType)
            addParameter("y", r.classType)
            addParameter("z", r.classType)
            returns(BOOLEAN)
            addCode("return (\n  x.created == EPOCH ||" + r.serde
                .filter { !listOf("id", "created", "updated").contains(it.name) }
                .joinToString(" ||") {
                    "\n  x.${it.name} != y.${it.name} && x.${it.name} != z.${it.name} && y.${it.name} != z.${it.name}"
                } + "\n)"
            )
        }

        addFunction("localPayload") {
            addModifiers(KModifier.OVERRIDE)
            addParameter("record", r.classType.nullable)
            addParameter("state", INT.nullable)
            returns(Types.ContentValues)
            addCode {
                addStatement("val values = ContentValues()")
                addControlFlow("record?.apply") {
                    r.serde.forEach {
                        addStatement(it.serializeLocal)
                    }
                }
                addControlFlow("if (state != null)") {
                    addStatement("values.put(\"$STATE\", state)")
                }
                addStatement("return values")
            }
        }

        addFunction("localDiff") {
            addModifiers(KModifier.OVERRIDE)
            addParameter("prev", r.classType)
            addParameter("curr", r.classType)
            returns(Types.ContentValues)
            addCode {
                addStatement("val values = ContentValues()")
                addControlFlow("with(curr)") {
                    r.serde.forEach {
                        addControlFlow("if (${it.name} != prev.${it.name})") {
                            addStatement(it.serializeLocal)
                        }
                    }
                }
                addStatement("return values")
            }
        }

        addFunction("remotePayload") {
            addModifiers(KModifier.OVERRIDE)
            addParameter("record", r.classType)
            returns(Types.MultipartBodyParts)
            addCode {
                addStatement("val values = mutableListOf<MultipartBody.Part>()")
                r.serde.forEach {
                    if (!listOf("created", "updated").contains(it.name)) {
                        addStatement("values.addParam(\"${it.name}\", ${it.serializeRemote})")
                    }
                }
                addStatement("return values")
            }
        }

        addFunction("remoteDiff") {
            addModifiers(KModifier.OVERRIDE)
            addParameter("prev", r.classType)
            addParameter("curr", r.classType)
            returns(Types.MultipartBodyParts)
            addCode {
                addStatement("val values = mutableListOf<MultipartBody.Part>()")
                r.serde.forEach {
                    if (!listOf("id", "created", "updated").contains(it.name)) {
                        addControlFlow("if (curr.${it.name} != prev.${it.name})") {
                            val serializeRemote = it.serializeRemote.replace("record", "curr")
                            addStatement("values.addParam(\"${it.name}\", ${serializeRemote})")
                        }
                    }
                }
                addStatement("return values")
            }
        }

        addFunction("fetchUpdates") {
            addModifiers(KModifier.SUSPEND, KModifier.OVERRIDE)
            addParameter("db", Types.PocketBase)
            addParameter("after", Types.Pair(Types.ZonedDateTime, STRING))
            returns(Types.List(r.classType).nullable)
            addCode("return db.getUpdates<${r.className}>(collection, after)")
        }

        addFunction("insertRemote") {
            addModifiers(KModifier.SUSPEND, KModifier.OVERRIDE)
            addParameter("db", Types.PocketBase)
            addParameter("payload", Types.MultipartBodyParts)
            returns(r.classType.nullable)
            addCode("return db.records.createRecord<${r.className}>(collection, body = payload).body()")
        }

        addFunction("updateRemote") {
            addModifiers(KModifier.SUSPEND, KModifier.OVERRIDE)
            addParameter("db", Types.PocketBase)
            addParameter("id", STRING)
            addParameter("payload", Types.MultipartBodyParts)
            returns(r.classType.nullable)
            addCode("return db.records.updateRecord<${r.className}>(collection, id, body = payload).body()")
        }
    }
}
