package dev.foxcroft.macros.generate


val String.plural: String
    get() {
        val lastTwo = substring(length - 2)
        val c = lastTwo[1]
        return if (c == 'y') {
            substring(0, length - 1) + "ies"
        } else if ("sxz".contains(c) || lastTwo == "ch" || lastTwo == "sh") {
            this + "es"
        } else {
            this + "s"
        }
    }

val String.lowercase get() = first().lowercase() + substring(1)
