package dev.foxcroft.macros.generate

import com.google.devtools.ksp.KspExperimental
import com.google.devtools.ksp.getAnnotationsByType
import com.google.devtools.ksp.symbol.KSAnnotated
import com.squareup.kotlinpoet.AnnotationSpec
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.CodeBlock
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.STRING
import com.squareup.kotlinpoet.TypeName
import com.squareup.kotlinpoet.TypeSpec
import dev.foxcroft.macros.adapterName


@OptIn(KspExperimental::class)
inline fun <reified T : Annotation> KSAnnotated.getAnnotation() =
    getAnnotationsByType(T::class).first()

inline fun fileSpec(packageName: String, fileName: String, block: FileSpec.Builder.() -> Unit) =
    FileSpec.builder(packageName, fileName).apply(block).build()

inline fun FileSpec.Builder.addClass(name: String, block: TypeSpec.Builder.() -> Unit) {
    addType(TypeSpec.classBuilder(name).apply(block).build())
}

inline fun FileSpec.Builder.addObject(name: String, block: TypeSpec.Builder.() -> Unit) {
    addType(TypeSpec.objectBuilder(name).apply(block).build())
}

inline fun TypeSpec.Builder.primaryConstructor(block: FunSpec.Builder.() -> Unit) {
    primaryConstructor(FunSpec.constructorBuilder().apply(block).build())
}

inline fun TypeSpec.Builder.addCompanionObject(block: TypeSpec.Builder.() -> Unit) {
    addType(TypeSpec.companionObjectBuilder().apply(block).build())
}

inline fun TypeSpec.Builder.addProperty(
    name: String,
    type: TypeName,
    vararg modifiers: KModifier,
    block: PropertySpec.Builder.() -> Unit,
) {
    addProperty(PropertySpec.builder(name, type, *modifiers).apply(block).build())
}

fun TypeSpec.Builder.addStringProperty(name: String, block: StringBuilder.() -> Unit) =
    addProperty(name, STRING, KModifier.OVERRIDE) {
        initializer("\"\"\"${buildString(block)}\"\"\"")
    }

inline fun TypeSpec.Builder.addFunction(name: String, block: FunSpec.Builder.() -> Unit) {
    addFunction(FunSpec.builder(name).apply(block).build())
}

inline fun FunSpec.Builder.addParameter(
    name: String,
    type: TypeName,
    block: ParameterSpec.Builder.() -> Unit
) {
    addParameter(ParameterSpec.builder(name, type).apply(block).build())
}

inline fun FunSpec.Builder.addCode(block: CodeBlock.Builder.() -> Unit) {
    addCode(CodeBlock.builder().apply(block).build())
}

inline fun CodeBlock.Builder.addControlFlow(
    controlFlow: String,
    block: CodeBlock.Builder.() -> Unit
) {
    beginControlFlow(controlFlow)
    apply(block)
    endControlFlow()
}

fun FileSpec.Builder.addOptInAnnotation() = addAnnotation(
    AnnotationSpec.builder(ClassName("kotlin", "OptIn")).apply {
        addMember("ExperimentalStdlibApi::class")
    }.build()
)

fun FileSpec.Builder.addLazyAdapter(
    moshi: String,
    className: String,
    i: Int,
    type: TypeName,
    typeStr: String,
) = addProperty(PropertySpec.builder(adapterName(className, i), Types.JsonAdapter(type)).apply {
    addModifiers(KModifier.PRIVATE)
    delegate("lazy { $moshi.adapter<$typeStr>() }")
}.build())
