package dev.foxcroft.macros.generate

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.LIST
import com.squareup.kotlinpoet.MAP
import com.squareup.kotlinpoet.MUTABLE_LIST
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.TypeName


@Suppress("FunctionName", "MemberVisibilityCanBePrivate")
object Types {
    fun List(vararg types: TypeName) = LIST.parameterizedBy(*types)
    fun Map(vararg types: TypeName) = MAP.parameterizedBy(*types)

    val Pair = ClassName("kotlin", "Pair")
    fun Pair(vararg types: TypeName) = Pair.parameterizedBy(*types)

    val MutableStateFlow = ClassName("kotlinx.coroutines.flow", "MutableStateFlow")
    fun MutableStateFlow(vararg types: TypeName) = MutableStateFlow.parameterizedBy(*types)

    val ZonedDateTime = ClassName("java.time", "ZonedDateTime")

    val JsonAdapter = ClassName("com.squareup.moshi", "JsonAdapter")
    fun JsonAdapter(vararg types: TypeName) = JsonAdapter.parameterizedBy(*types)

    val MultipartBodyParts =
        MUTABLE_LIST.parameterizedBy(ClassName("okhttp3", "MultipartBody", "Part"))

    val ContentValues = ClassName("android.content", "ContentValues")
    val Context = ClassName("android.content", "Context")
    val Cursor = ClassName("android.database", "Cursor")
    val ConnectivityManager = ClassName("android.net", "ConnectivityManager")

    private const val root = "dev.foxcroft.pocketbase"

    val AppConfig = ClassName(root, "AppConfig")

    val PbBaseViewModel = ClassName(root, "PbBaseViewModel")
    fun PbBaseViewModel(vararg types: TypeName) = PbBaseViewModel.parameterizedBy(*types)

    val AuthUseCase = ClassName("$root.auth", "AuthUseCase")
    fun AuthUseCase(vararg types: TypeName) = AuthUseCase.parameterizedBy(*types)

    val DatabaseHelper = ClassName("$root.db", "DatabaseHelper")

    val RtEvent = ClassName("$root.realtime", "RtEvent")
    fun RtEvent(vararg types: TypeName) = RtEvent.parameterizedBy(*types)

    val PocketBase = ClassName("$root.retrofit", "PocketBase")

    val CollectionSyncHelper = ClassName("$root.sync", "CollectionSyncHelper")
    fun CollectionSyncHelper(vararg types: TypeName) = CollectionSyncHelper.parameterizedBy(*types)

    val SyncHelper = ClassName("$root.sync", "SyncHelper")
    fun SyncHelper(vararg types: TypeName) = SyncHelper.parameterizedBy(*types)
}

val TypeName.nullable get() = copy(nullable = true)
