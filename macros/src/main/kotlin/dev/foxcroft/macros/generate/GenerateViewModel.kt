package dev.foxcroft.macros.generate

import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.squareup.kotlinpoet.ANY
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.STAR
import com.squareup.kotlinpoet.STRING
import com.squareup.kotlinpoet.UNIT
import com.squareup.kotlinpoet.ksp.toClassName
import com.squareup.kotlinpoet.ksp.toTypeName

fun generateMoshiImpl(
    moshi: KSPropertyDeclaration,
    authRecords: List<KSClassDeclaration>,
    records: List<RecordDef>,
): FileSpec {
    val moshiName = moshi.simpleName.asString()
    val packageName = moshi.packageName.asString()
    val authRecordClassDef = authRecords.firstOrNull()
    val authRecord = authRecordClassDef?.toClassName() ?: UNIT
    val authCollection = authRecordClassDef?.getAnnotation<PocketbaseRecord>()?.collection
    return fileSpec(packageName, "_pb") {
        addOptInAnnotation()
        addImport("com.squareup.moshi", "adapter")

        records.forEachIndexed { i, it ->
            addImport(it.packageName, it.className)
            val type = Types.RtEvent(it.classType)
            val typeStr = "RtEvent<${it.className}>"
            addLazyAdapter(moshiName, "_", i, type, typeStr)
        }

        val superType = if (records.size > 1) {
            records
                .map { it.classDef.superTypes.map { t -> t.toTypeName() }.toSet() }
                .reduce { acc, set -> acc.intersect(set) }
                .firstOrNull()
        } else {
            records.firstOrNull()?.classType
        }

        addAliasedImport(Types.CollectionSyncHelper, "CSH")
        addClass("PbViewModel") {
            primaryConstructor {
                addModifiers(KModifier.PRIVATE)
                addParameter("connectivityManager", Types.ConnectivityManager)
                addParameter("db", Types.DatabaseHelper)
                addParameter("service", Types.PocketBase)
                addParameter("authUseCase", Types.AuthUseCase(authRecord))
                records.forEach {
                    addParameter(it.helper.lowercase, Types.CollectionSyncHelper(it.classType)) {
                        defaultValue("CSH(${it.helper}, db, service)")
                    }
                }
                val type = Types.List(Types.CollectionSyncHelper(STAR))
                addParameter("collectionSyncHelpers", type) {
                    defaultValue("listOf(${records.joinToString(", ") { it.helper.lowercase }})")
                }
            }
            records.forEach {
                val name = it.helper.lowercase
                addProperty(name, Types.CollectionSyncHelper(it.classType)) {
                    addModifiers(KModifier.PRIVATE)
                    initializer(name)
                }
            }
            addProperty("collectionSyncHelpers", Types.List(Types.CollectionSyncHelper(STAR))) {
                addModifiers(KModifier.OVERRIDE)
                initializer("collectionSyncHelpers")
            }
            superclass(Types.PbBaseViewModel(authRecord, superType ?: ANY))
            addSuperclassConstructorParameter("connectivityManager")
            addSuperclassConstructorParameter("db")
            addSuperclassConstructorParameter("service")
            addSuperclassConstructorParameter("authUseCase")
            addCompanionObject {
                addFunction("create") {
                    addParameter("context", Types.Context)
                    addParameter("config", Types.AppConfig)
                    returns(ClassName(packageName, "PbViewModel"))
                    val names = records.joinToString(", ") { it.helper }
                    addCode("return create(context, config, moshi, ::PbViewModel, \"$authCollection\", listOf($names))")
                }
            }
            records.forEach {
                val mapType = Types.Map(STRING, it.classType)
                val type = Types.MutableStateFlow(mapType)
                addProperty(it.className.lowercase.plural, type) {
                    initializer("${it.helper.lowercase}.objects")
                }
            }
            records.forEach {
                addFunction("putLocal") {
                    addParameter("prev", it.classType.nullable)
                    addParameter("record", it.classType)
                    addCode("${it.helper.lowercase}.putLocal(prev, record)")
                }
            }
            addFunction("parseEvent") {
                addModifiers(KModifier.OVERRIDE)
                addParameter("event", STRING)
                addParameter("payload", STRING)
                returns(Types.RtEvent(superType ?: ANY))
                addCode {
                    if (records.size > 1) {
                        addControlFlow("return when (event)") {
                            records.forEachIndexed { i, it ->
                                addStatement("\"${it.collection}\" -> _Adapter$i.fromJson(payload)!!")
                            }
                            addStatement("else -> throw IllegalArgumentException(event)")
                        }
                    } else {
                        addStatement("return _Adapter0.fromJson(payload)!!")
                    }
                }
            }
            addFunction("onEvent") {
                addModifiers(KModifier.OVERRIDE)
                addParameter("it", Types.RtEvent(superType ?: ANY))
                addCode {
                    if (records.size > 1) {
                        addControlFlow("return when (val record = it.record)") {
                            records.forEach {
                                addStatement("is ${it.className} -> ${it.helper.lowercase}.putRemote(record)")
                            }
                        }
                    } else {
                        addStatement("return ${records[0].helper.lowercase}.putRemote(it.record)")
                    }
                }
            }
        }
    }
}
