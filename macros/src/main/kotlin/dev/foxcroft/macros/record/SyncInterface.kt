package dev.foxcroft.macros.record

import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime

const val STATE = "_state"
const val BASE = 0
const val REMOTE = 1
const val LOCAL = 2

val EPOCH: ZonedDateTime = ZonedDateTime.ofInstant(Instant.EPOCH, ZoneId.of("Z"))

data class Record<T>(
    val base: T?,
    val remote: T?,
    val local: T?,
)

interface SyncInterface<LocalDatabase, LocalPayload, RemoteDatabase, RemotePayload, T> {
    val collection: String

    fun id(item: T): String

    fun updated(item: T): ZonedDateTime

    fun conflictBase(item: T): T

    fun areEqual(self: T, other: T): Boolean

    fun hasConflict(x: T, y: T, z: T): Boolean

    fun localPayload(record: T? = null, state: Int? = null): LocalPayload

    fun localDiff(prev: T, curr: T): LocalPayload

    fun remotePayload(record: T): RemotePayload

    fun remoteDiff(prev: T, curr: T): RemotePayload

    fun getRecord(db: LocalDatabase, id: String): Record<T>

    fun getItems(db: LocalDatabase): List<T>

    fun getUpdates(db: LocalDatabase): List<Record<T>>

    fun getConflicts(db: LocalDatabase): List<Record<T>>

    fun insert(db: LocalDatabase, item: T, state: Int)

    fun update(db: LocalDatabase, values: LocalPayload, id: String, state: Int?)

    fun delete(db: LocalDatabase, id: String, states: List<Int>?)

    fun setSyncState(db: LocalDatabase, state: Pair<ZonedDateTime, String>)

    fun <T> transaction(db: LocalDatabase, block: LocalDatabase.() -> T) = db.block()

    suspend fun fetchUpdates(db: RemoteDatabase, after: Pair<ZonedDateTime, String>): List<T>?

    suspend fun insertRemote(db: RemoteDatabase, payload: RemotePayload): T?

    suspend fun updateRemote(db: RemoteDatabase, id: String, payload: RemotePayload): T?

    fun putLocal(db: LocalDatabase, prev: T?, item: T): Record<T> = transaction(db) {
        val id = id(item)
        if (prev != null && areEqual(prev, item)) {
            return@transaction getRecord(this, id)
        }
        val (base, remote, local) = getRecord(this, id)
        if (remote == null && local == null) {
            if (prev == null) {
                insert(this, item, LOCAL)
            } else {
                throw IllegalStateException("Cannot edit a record that was deleted")
            }
        } else if (prev == null) {
            throw IllegalStateException("Cannot insert record that already exists")
        } else if (prev == base) {
            if (remote == null) {
                throw IllegalStateException("Invalid database state")
            } else if (!areEqual(item, remote)) {
                delete(this, id, listOf(BASE))
                update(this, localPayload(item), id, LOCAL)
            } else {
                delete(this, id, listOf(BASE, LOCAL))
            }
        } else if (local != null) {
            if (!hasConflict(item, prev, local)) {
                update(this, localDiff(prev, item), id, LOCAL)
            } else if (base != null) {
                throw IllegalStateException("Cannot make conflicting edit to conflicted record")
            } else {
                throw IllegalStateException("Cannot make conflicting edit to local-only record")
            }
        } else {
            if (prev == remote!!) {
                insert(this, item, LOCAL)
            } else if (!hasConflict(item, prev, remote)) {
                insert(this, remote, LOCAL)
                update(this, localDiff(prev, item), id, LOCAL)
            } else {
                insert(this, prev, BASE)
                insert(this, item, LOCAL)
            }
        }
        getRecord(this, id)
    }

    fun putRemote(db: LocalDatabase, item: T): Record<T> = transaction(db) {
        val id = id(item)
        val (base, remote, local) = getRecord(this, id)
        if (local == null || areEqual(item, local)) {
            delete(this, id, null)
            insert(this, item, REMOTE)
        } else if (base != null) {
            update(this, localPayload(item), id, REMOTE)
            if (!hasConflict(base, item, local)) {
                update(this, localDiff(base, item), id, LOCAL)
                delete(this, id, listOf(BASE))
            }
        } else if (remote == null) {
            insert(this, item, REMOTE)
            insert(this, conflictBase(item), BASE)
        } else if (hasConflict(item, remote, local)) {
            update(this, localPayload(state = BASE), id, REMOTE)
            insert(this, item, REMOTE)
        } else {
            update(this, localDiff(remote, item), id, null)
        }
        getRecord(this, id)
    }

    suspend fun pull(
        db: LocalDatabase,
        service: RemoteDatabase,
        initialSyncState: Pair<ZonedDateTime, String>?,
        onChange: (List<Pair<String, Record<T>>>) -> Unit,
    ): Boolean {
        var syncState = initialSyncState ?: Pair(EPOCH, "")
        while (true) {
            val items = fetchUpdates(service, syncState) ?: return false
            val records = transaction(db) {
                items.lastOrNull()?.let { last ->
                    syncState = Pair(updated(last), id(last))
                    setSyncState(this, syncState)
                }
                items.map { Pair(id(it), putRemote(this, it)) }
            }
            onChange(records)
            if (items.size < 100) {
                return true
            }
        }
    }

    suspend fun push(db: LocalDatabase, service: RemoteDatabase) {
        for (record in getUpdates(db)) {
            val local = record.local!!
            val remote = record.remote
            val response = if (remote == null) {
                val body = remotePayload(local)
                insertRemote(service, body)
            } else {
                val body = remoteDiff(remote, local)
                updateRemote(service, id(local), body)
            }
            if (response != null) {
                transaction(db) {
                    putRemote(this, response)
                }
            }
        }
    }
}
