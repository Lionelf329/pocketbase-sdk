package dev.foxcroft.macros.record

import com.google.devtools.ksp.getDeclaredProperties
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import dev.foxcroft.macros.Processor

fun Processor.isValidRecord(classDef: KSClassDeclaration): Boolean {
    val fields = classDef.getDeclaredProperties().toList()
    if (!isValidProperty(fields[0], "id", "kotlin.String")) {
        return false
    }
    if (!isValidProperty(fields[1], "created", "java.time.ZonedDateTime")) {
        return false
    }
    if (!isValidProperty(fields[2], "updated", "java.time.ZonedDateTime")) {
        return false
    }
    if (fields.any { it.simpleName.asString() == STATE }) {
        environment.logger.warn("Pocketbase records cannot define a property named $STATE")
        return false
    }
    return true
}

fun Processor.isValidProperty(
    property: KSPropertyDeclaration,
    name: String,
    typeName: String
): Boolean {
    if (property.simpleName.asString() != name) {
        environment.logger.warn("Expected $name, got ${property.simpleName.asString()}")
        return false
    }
    val type = property.getTypeName()
    if (type != typeName) {
        environment.logger.warn("Expected $name to be $typeName got $type")
        return false
    }
    return true
}

fun KSPropertyDeclaration.getTypeName() =
    type.resolve().declaration.qualifiedName?.asString() ?: "unknown"
