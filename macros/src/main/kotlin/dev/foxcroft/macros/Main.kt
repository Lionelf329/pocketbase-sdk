package dev.foxcroft.macros

import com.google.devtools.ksp.containingFile
import com.google.devtools.ksp.processing.Dependencies
import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.processing.SymbolProcessor
import com.google.devtools.ksp.processing.SymbolProcessorEnvironment
import com.google.devtools.ksp.processing.SymbolProcessorProvider
import com.google.devtools.ksp.symbol.KSAnnotated
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.ksp.writeTo
import dev.foxcroft.macros.generate.generateMoshiImpl
import dev.foxcroft.macros.generate.PocketbaseRecord
import dev.foxcroft.macros.generate.RecordDef
import dev.foxcroft.macros.generate.generateRecordImpl
import dev.foxcroft.macros.generate.getAnnotation
import dev.foxcroft.macros.record.isValidRecord

@Target(AnnotationTarget.PROPERTY)
annotation class PocketbaseMoshi

class Provider : SymbolProcessorProvider {
    override fun create(environment: SymbolProcessorEnvironment) = Processor(environment)
}

class Processor(val environment: SymbolProcessorEnvironment) : SymbolProcessor {
    override fun process(resolver: Resolver): List<KSAnnotated> {
        val moshi = resolver.get<PocketbaseMoshi, KSPropertyDeclaration>().firstOrNull()
        val allRecords = resolver.get<PocketbaseRecord, KSClassDeclaration>().toList()
        val authRecords = allRecords.filter { it.getAnnotation<PocketbaseRecord>().isAuth }
        val records = allRecords.filter(::isValidRecord).map(RecordDef::create)
        if (moshi == null) {
            if (records.isNotEmpty()) {
                val msg = "Annotate your moshi instance with @PocketbaseMoshi to generate helpers"
                environment.logger.warn(msg)
            }
            return emptyList()
        }
        val moshiLocation = ClassName(moshi.packageName.asString(), moshi.simpleName.asString())
        write(moshi, generateMoshiImpl(moshi, authRecords, records))
        records.forEach {
            write(it.classDef, generateRecordImpl(it, moshiLocation))
        }
        return emptyList()
    }

    private fun write(source: KSAnnotated, fileSpec: FileSpec) {
        val dependencies = Dependencies(aggregating = false, source.containingFile!!)
        fileSpec.writeTo(environment.codeGenerator, dependencies)
    }

    private inline fun <reified A : Annotation, reified T> Resolver.get() =
        getSymbolsWithAnnotation(A::class.qualifiedName!!).filterIsInstance<T>()
}
