plugins {
    id("org.jetbrains.kotlin.jvm")
}

dependencies {
    implementation("com.google.devtools.ksp:symbol-processing-api:1.9.22-1.0.16")
    implementation("com.squareup:kotlinpoet-ksp:1.15.3")
}
